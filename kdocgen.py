#!/usr/bin/python3

"""
Script for KiCad documentation automation
Based on kicad_7_doc_gen.py by Jordan Aceto
https://github.com/JordanAceto/kicad_7_cli_doc_gen

Rich Holmes / March 2023
"""

import os
import sys
import argparse
import fnmatch
import json
from copy import deepcopy
from KiCadProj import KiCadProj
import subprocess

steps = ["schem_pdf", "ibom", "bom", "pcb_svg", "pcb_pdf", "gerbers", "step", "readme"]

def find_kdirs(pattern: str, path: str) -> list[str]:
    """`find_kdirss(pattern, path)` returns paths containing files matching pattern `pattern`."""
    ret = []
    for root, _, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                ret.append(root)
    return ret

def merge_params (ps1: dict[str], ps2: dict[str]) -> dict[str] :
    """`merge_params (ps1, ps2)` updates parameter structure ps1 with ps2 and returns it"""
    ps1a = deepcopy(ps1)
    ps2a = deepcopy(ps2)

    # First handle obsolete "enabled" list
    
    if "enabled" in ps2a:
        for e in steps:
            if e not in ps2a:
                ps2a[e] = {}
                if "enabled" not in ps2a[e] or ps2a[e]["enabled"] == None:
                    ps2a[e]["enabled"] = e in ps2a["enabled"]

    # Now the actual merge
    
    for entry in ps2a:
        if entry == "enabled":
            continue
        if entry not in ps1a:
            print (f"***** Unknown configuration file entry '{entry}' ignored")
            continue
        if entry in steps:
            for entry2 in ps2a[entry]:
                if entry2 not in ps1a[entry]:
                    print (f"***** Unknown configuration file entry '{entry2}' in '{entry}' ignored")
                    continue
                ps1a[entry][entry2] = ps2a[entry][entry2]
        else:
            ps1a[entry] = ps2a[entry]
            
    return ps1a

def sch_do (k_proj: KiCadProj, docheck: bool) -> None:
    """Process sch dependent stuff"""
    k_proj.schem_pdf(docheck)
    k_proj.bom(docheck)
    return

def pcb_do (k_proj: KiCadProj, docheck: bool) -> None:
    """Process PCB dependent stuff, `sep` if doing separation."""
    k_proj.pcb_2D_layers(docheck)
    k_proj.ibom(docheck)
    k_proj.gerbers(docheck)
    k_proj.step(docheck)
    return

def readme_start (proj_dir: str, proj_name: str) -> str:
    """Returns starting content for a basic README, if no README file already exists, else "" """
    readme_exists = any([os.path.isfile(f"{proj_dir}/README.md"),
                         os.path.isfile(f"{proj_dir}/Readme.md"),
                         os.path.isfile(f"{proj_dir}/readme.md")])
    if not readme_exists:
        print("*** Generating basic README file")
        return f"# {proj_name}\n\n"
    else:
        return ""

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        'project_dir',
        type=str,
        help='path to directory encompassing one or more KiCad projects'
    )
    parser.add_argument(
        '-v', '--version',
        action='version',
        version="%(prog)s v0.2.1"
    )
    parser.add_argument(
        '-p', '--params',
        action='store_true',
        help='show parameters for each KiCad project and exit'
    )
    parser.add_argument(
        '-c', '--cont',
        action='store_true',
        help='continue processing after error'
    )
    parser.add_argument(
        '-f', '--filesmade',
        type=str,
        help='file to which to write list of files created'
        )
    parser.add_argument(
        '-fi', '--filesmade_ignore',
        type=str,
        help='like --filesmade but omit files ignored by git'
        )
    args = parser.parse_args()

    docheck = not args.cont

    fm = args.filesmade
    fmi = False
    if not fm:
        fm = args.filesmade_ignore
        if fm:
            fmi = True
    if fm:
        if os.path.exists(fm):
            if os.path.isfile(fm):
                os.rename(fm, f"{fm}.bak")
            else:
                sys.exit (f"*** Cannot write file {fm}, exists and is not file")

    proj_name = os.path.basename(os.path.realpath(args.project_dir))
    proj_dir = args.project_dir.strip("/ ")
    if not os.path.isdir (proj_dir):
        sys.exit (f"*** Project directory not found: {proj_dir}")        
    kproj_dir_list = find_kdirs("*.kicad_pro", proj_dir)
    if kproj_dir_list == []:
        sys.exit (f"*** No k-projects found")
    params = {
        "docs_dir": "docs",
        "2d_dir": "2D",
        "3d_dir": "3D",
        "bom_dir": "BOM",
        "gerb_dir": "Gerbers",
        "kicad_cli_cmd": "kicad-cli",
        "kikit_path": "/usr/local/bin/kikit",
        "separation": [],
        "schem_pdf": {"enabled": True},
        "ibom": {
            "enabled": True,
            "path": "/usr/local/bin/generate_interactive_bom.py",
            "dark_mode": True
        },
        "bom": {
            "enabled": True,
            "path": "/usr/share/kicad/plugins/bom_csv_grouped_extra.py",
            "args" : "",
            "ext" : "csv"
        },
        "pcb_svg": {
            "enabled": True,
            "front_layers": ["Edge.Cuts","F.SilkS","F.Cu"],
            "back_layers": ["Edge.Cuts","B.SilkS","B.Cu"],
            "theme": "",
            "black_and_white": False,
            "page_size_mode": 2,
            "exclude_drawing_sheet": True
        },
        "pcb_pdf": {
            "enabled": True,
            "front_layers": ["F.Cu", "In1.Cu", "In2.Cu", "F.SilkS", "F.Paste", "F.Mask"],
            "back_layers": ["B.Cu", "B.SilkS", "B.Paste", "B.Mask"],
            "edge_layer": "Edge.Cuts",
            "exclude_refdes": True,
            "exclude_value": True,
            "include_border_title": True,
            "theme": "",
            "black_and_white": True
        },
        "gerbers": {
            "enabled": True,
            "kikit": False,
            "compress": 2,
            "layers": ["F.Cu", "In1.Cu", "In2.Cu", "B.Cu", "F.SilkS", "B.SilkS", "F.Paste", "B.Paste", "F.Mask", "B.Mask", "Edge.Cuts"],
            "exclude_refdes": False,
            "exclude_value": False,
            "include_border_title": False,
            "no-protel-ext": False,
            "fabhouse": "jlcpcb",
            "drc": True,
            "options": ""
        },
        "step": {"enabled": True},
        "readme": {"enabled": True}
    }

    # Update params from global and semilocal rc files
    # Look for unhidden files first, then hidden

    home_directory = os.path.expanduser( '~' )
    global_rc_path = [os.path.join (home_directory, 'kdocgenrc.json'),
                      os.path.join (home_directory, '.kdocgenrc.json')]
    semilocal_rc_path = [os.path.join (proj_dir, 'kdocgenrc.json'),
                         os.path.join (proj_dir, '.kdocgenrc.json')]
    
    for rc_path in [global_rc_path, semilocal_rc_path]:
        params_override = ""
        try:
            with open (rc_path[0], "r") as f:
                try:
                    params_override = json.load (f)
                except Exception as e:
                    print (f"*** Failed to load {rc_path[0]}")
                    print (e)
                    exit()
                if rc_path[0] != global_rc_path[0]:
                    print (f"*** Updating params from {rc_path[0]}")
                idx = 0
        except Exception:
            try:
                with open (rc_path[1], "r") as f:
                    try:
                        params_override = json.load (f)
                    except Exception as e:
                        print (f"*** Failed to load {rc_path[1]}")
                        print (e)
                        exit()
                    if rc_path[1] != global_rc_path[1]:
                        print (f"*** Updating params from {rc_path[1]}")
                    idx = 1
            except Exception:
                pass

        if params_override != "":
            if "enabled" in params_override:
                print (f"*****  'enabled' in {rc_path[idx]} is deprecated")
            params = merge_params (params, params_override)

    readme_content = readme_start (proj_dir, proj_name) if params["readme"]["enabled"] else ""
    doreadme = readme_content != ""
    
    # Process each k-project

    filelist = []
    for kproj_dir in kproj_dir_list:
        print (f"*** Processing {kproj_dir}")
        params_override = ""
        kparams = dict(params)
        if kproj_dir != proj_dir:
            local_rc_path = [os.path.join (kproj_dir, 'kdocgenrc.json'),
                             os.path.join (kproj_dir, '.kdocgenrc.json')]
            try:
                with open (local_rc_path[0], "r") as f:
                    try:
                        params_override = json.load (f)
                    except Exception as e:
                        print (f"*** Failed to load {local_rc_path[0]}")
                        print (e)
                        exit()
                    print (f"*** Updating params for this k-project from {local_rc_path[0]}")
                    idx = 0
            except Exception:
                try:
                    with open (local_rc_path[1], "r") as f:
                        try:
                            params_override = json.load (f)
                        except Exception as e:
                            print (f"*** Failed to load {local_rc_path[1]}")
                            print (e)
                            exit()
                        print (f"*** Updating params for this k-project from {local_rc_path[1]}")
                        idx = 1
                except Exception:
                    pass

            if params_override != "":
                if "enabled" in params_override:
                    print (f"*****  'enabled' in {local_rc_path[idx]} is deprecated")
                kparams = merge_params (kparams, params_override)
        
        if args.params:
            print (json.dumps(kparams, indent=4))
            continue

        with KiCadProj(proj_dir, proj_name, kproj_dir, kparams, fmi) as k_proj:

            # generate the outputs

            sch_do (k_proj, docheck)
            
            if len(k_proj.params["separation"]) == 0:
                pcb_do(k_proj, docheck)
                if doreadme:
                    readme_content += k_proj.readme()
            else:
                for isep in range(len(k_proj.params["separation"])):
                    k_proj.next_separation()
                    pcb_do(k_proj, docheck)
                    if doreadme:
                        readme_content += k_proj.readme()

            filelist += k_proj.filelist
            
    if doreadme:
        with open(f"{proj_dir}/README.md", "w") as f:
            f.write (readme_content)
            did = True
            fn = f"{proj_dir}/README.md"
            if fmi:
                try:
                    result = subprocess.run(['git', 'check-ignore', '-q', fn], \
                                        capture_output=True, text=True)
                    if result.returncode == 0:
                        fn = ""
                except FileNotFoundError:
                    print("*** Git is not installed or not available in PATH.")
            if fn:
                filelist.append(fn)

    if filelist == []:
        print ("*** Nothing to do!")

    if fm:
        try:
            with open(fm, "w") as f:
                for fl in filelist:
                    print (fl, file=f)
        except:
            print (f"*** Can't write {fm}")
        
if __name__ == '__main__':
    main()
