import fnmatch
import os
import shutil
import subprocess
import sys

def find_file(pattern: str, path: str) -> str:
    """`find_file(pattern, path)` returns first file found matching pattern `pattern`."""
    for root, _, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                return (os.path.join(root, name))

def set_dir (dir: str, pd: str, kpd: str) -> str:
    # `set_dir (dir, pd, kpd)` sets returns path for `dir` relative to `pd`, `kpd`, or `kpd`'s parent
    ret = ""
    if dir[:7] == "{KPROJ}":
        if len(dir) == 7:
            ret = kpd
        elif dir[7] == "/":
            if kpd == "":
                ret = f"{dir[8:]}"
            else:
                ret = f"{kpd}/{dir[8:]}"
        else:
            raise (Subst_Error(f"Invalid substitution in {dir}"))

    elif dir[:9] == "{KPROJUP}":
        if kpd == pd:
            raise (Subst_Error(f"Cannot substitute parent of {kpd} in {dir}"))
        kpdu = os.path.dirname(kpd)
        if len(dir) == 9:
            ret = kpdu
        elif dir[9] == "/":
            if kpdu == "":
                ret = f"{dir[8:]}"
            else:
                ret = f"{kpdu}/{dir[8:]}"
        else:
            raise (Subst_Error(f"Invalid substitution in {dir}"))

    elif dir[:6] == "{PROJ}":
        if len(dir) == 6:
            ret = pd
        elif dir[6] == "/":
            if pd == "":
                ret = f"{dir[8:]}"
            else:
                ret = f"{pd}/{dir[8:]}"
        else:
            raise (Subst_Error(f"Invalid substitution in {dir}"))

    else:
        if pd == "":
            ret = dir
        else:
            ret = f"{pd}/{dir}"

    ret = ret.strip("/ ")
    while "//" in ret:
        ret = ret.replace("//", "/")

    return ret

def get_kproject_name(path: str) -> str:
    """`get_kproject_name(path)` is the name of the KiCad project in the given path."""
    kproject_file = find_file("*.kicad_pro", path)
    kproject_name = kproject_file.split(".kicad_pro")[0]
    kproject_name = kproject_name.split("/")[-1]
    return kproject_name

class Subst_Error(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return(repr(self.value))

class KiCadProj():
    """A KiCad project to be processed"""
    def __init__(self, path: str, projname: str, kpath: str, params: dict[str], \
                 ignored: bool):
        self.proj_dir = path
        self.params = params
        # PCB file info
        # There can be multiple PCB files, first is primary
        self.pcbi = 0 # index
        self.pcb_exists = [None, None] # base and separation
        self.proj_name = projname
        self.kproj_name = get_kproject_name(kpath)
        self.kproj_dir = kpath
        self.pcb_file = f"{kpath}/{self.kproj_name}.kicad_pcb"
        self.pcb_stamp = 0 # single stamp for primary file

        # Sch file info
        self.sch_exists = None
        k_filename = self.kproj_name+".kicad_sch"
        self.sch_file = f"{self.kproj_dir}/{k_filename}"
        self.sch_stamp = 0

        # Output directories
        self.docs_dir = set_dir (self.params['docs_dir'], self.proj_dir, self.kproj_dir)
        self.gerb_dir = set_dir (self.params['gerb_dir'], self.proj_dir, self.kproj_dir)
        self.twod_dir = f"{self.docs_dir}/{self.params['2d_dir']}".strip("/ ")
        self.threed_dir = f"{self.docs_dir}/{self.params['3d_dir']}".strip("/ ")
        self.bom_dir = f"{self.docs_dir}/{self.params['bom_dir']}".strip("/ ")

        # Results
        self.filelist = []  # List of files created (or re-created)
        self.filelist_ignore = ignored # Whether to omit files ignored by git
        
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        for sep in range (len(self.params["separation"])):
            skd = self.sep_k_dir(sep+1)
            if os.path.isdir(skd) and "temp" in skd:
                shutil.rmtree(skd)
                print (f"*** Removed {skd}")
        return

    def enabled (self, thing: str) -> bool:
        """`enabled` checks if `thing` is enabled"""
        return self.params[thing]["enabled"]

    def next_separation (self) -> None:
        """`next_separation` sets up to process the next separation"""
        self.pcbi += 1
        self.pcb_exists[1] = None

    def sep_k_name (self, pcbi: int) -> str:
        """`sep_k_name` returns the base k-project name if `pcbi`==0, else k-project name for separation # `pcbi`"""
        if pcbi == 0:
            return self.kproj_name
        else:
            return f"{self.kproj_name}_{self.params['separation'][pcbi-1][0]}"

    def sep_k_dir (self, pcbi: int) -> str:
        """`sep_k_dir` returns the base k-project path if `pcbi`==0, else k-project path for separation # `pcbi`"""
        if pcbi == 0:
            return self.kproj_dir
        else:
            return f"{self.kproj_dir}/temp_{self.params['separation'][pcbi-1][0]}"

    def sep_k_pcb_file (self, pcbi: int) -> str:
        """`sep_k_pcb_file` returns the base k-project PCB file path if `pcbi`==0, else k-project PCB file path for separation # `pcbi`"""
        return f"{self.sep_k_dir(pcbi)}/{self.sep_k_name(pcbi)}.kicad_pcb"

    def go_ahead (self, path: str, sch: bool, docheck: bool) -> bool :
        """`go_ahead(path, stamp)` returns True if file or directory at `path`, which depends on .kicad_sch if `sch` else .kicad_pro, can and should be regenerated"""
        
        if sch:
            check = self.check_sch_file()
            stamp = self.sch_stamp
        else:
            check = self.check_pcb_file(True, docheck)
            stamp = self.pcb_stamp
        if not check:
            return False
        if os.path.exists(path) and os.path.getmtime(path) > stamp:
            return False
        if sch:
            return True
        else:
            return self.check_pcb_file(False, docheck)

    def kdreplace (self, thestring: str) -> str:
        """`kdreplace (self, thestring)` performs substitutions on `thestring`, then ensures the resulting path exists"""
        ret = thestring
        ret = ret.replace("{DOCS}", self.docs_dir)
        ret = ret.replace("{2D}", self.twod_dir)
        ret = ret.replace("{3D}", self.threed_dir)
        ret = ret.replace("{BOM}", self.bom_dir)
        ret = ret.replace("{GERBERS}", self.gerb_dir)
        ret = ret.replace("{SCH}", self.sch_file)
        ret = ret.replace("{PCB}", self.sep_k_pcb_file(self.pcbi))
        ret = ret.replace("{KNAME}", self.sep_k_name(self.pcbi))
        ret = ret.replace("{NAME}", self.proj_name)
        if self.pcbi > 0:
            ret = ret.replace("{SEP}", self.params['separation'][self.pcbi-1][0])

        return ret

    def kdocpath (self, path: str, docheck: bool) -> str:
        """`kdocpath (self, path)` performs substitutions on `path`, then ensures the resulting path exists"""
        ret = self.kdreplace(path)
        if not os.path.realpath(self.proj_dir) in os.path.realpath(ret):
            print (f"*** Path out of bounds: {ret}")
            if docheck:
                sys.exit("*** Aborting")
            return None
        if "{" in ret or "}" in ret:
            print (f"*** Invalid character in {ret}")
            if docheck:
                sys.exit("*** Aborting")
            return None
        if not os.path.isdir (os.path.dirname(ret)):
            os.makedirs (os.path.dirname(ret))
            if not os.path.isdir (os.path.dirname(ret)):
                print (f"*** Cannot create path {ret}")
                if docheck:
                    sys.exit("*** Aborting")
                return None
        return ret

    def appfile (self, file_path):
        if self.filelist_ignore:
            try:
                result = subprocess.run(['git', 'check-ignore', '-q', file_path], \
                                        capture_output=True, text=True)
                if result.returncode == 0:
                    return
            except FileNotFoundError:
                print("*** Git is not installed or not available in PATH.")
        self.filelist.append (file_path)        
    
    def schem_pdf(self, docheck: bool) -> None:
        # TODO default color and fonts are used, I want it to plot the user theme/fonts
        if not self.enabled ("schem_pdf"):
            return
        file_path = self.kdocpath(f"{{DOCS}}/{{KNAME}}_schematic.pdf", docheck)
        if file_path != None and self.go_ahead (file_path, True, docheck):
            print ("*** Doing PDF schematic")
            subprocess.run(self.kdreplace(
                f"{self.params['kicad_cli_cmd']} sch export pdf -o '{file_path}' '{{SCH}}'"), shell=True, check=docheck)
            self.appfile (file_path)
        return

    def pcb_2D_layers(self, docheck: bool) -> None:
        # PCB front and back as SVGs
        if self.enabled ("pcb_svg"):
            doing = "*** Doing PCB layout as SVG"
            theme = "--theme " + self.params["pcb_svg"]["theme"] if self.params["pcb_svg"]["theme"] else ""
            bandw = "--black-and-white" if self.params["pcb_svg"]["black_and_white"] else ""
            eds  = "--exclude-drawing-sheet" if self.params["pcb_svg"]["exclude_drawing_sheet"] else ""
            front_path = self.kdocpath(f"{{2D}}/{{KNAME}}_front.svg", docheck)
            if front_path != None and self.go_ahead (front_path, False, docheck):
                print (doing)
                doing = ""
                front_layers_as_str = ",".join(self.params["pcb_svg"]["front_layers"])
                subprocess.run(self.kdreplace(
                    f"{self.params['kicad_cli_cmd']} pcb export svg --page-size-mode {self.params['pcb_svg']['page_size_mode']} {eds} --layers {front_layers_as_str} {theme} {bandw} -o '{front_path}' '{{PCB}}'"), shell=True, check=docheck)
                print ()
                self.appfile (front_path)
            back_path = self.kdocpath (f"{{2D}}/{{KNAME}}_back.svg", docheck)
            if back_path != None and self.go_ahead (back_path, False, docheck):
                if doing:
                    print (doing)
                back_layers_as_str = ",".join(self.params["pcb_svg"]["back_layers"])
                subprocess.run(self.kdreplace(
                    f"{self.params['kicad_cli_cmd']} pcb export svg --page-size-mode {self.params['pcb_svg']['page_size_mode']} {eds} --mirror --layers {back_layers_as_str} {theme} {bandw} -o '{back_path}' '{{PCB}}'"), shell=True, check=docheck)
                print ()
                self.appfile (back_path)

        # all the board layers as individual PDFs
        if self.enabled ("pcb_pdf"):
            doing = "*** Doing PCB layout as PDF"
            theme = "--theme " + self.params["pcb_pdf"]["theme"] if self.params["pcb_pdf"]["theme"] else ""
            bandw = "--black-and-white" if self.params["pcb_pdf"]["black_and_white"] else ""
            er = "--exclude-refdes" if self.params["pcb_pdf"]["exclude_refdes"] else ""
            ev = "--exclude-value" if self.params["pcb_pdf"]["exclude_value"] else ""
            ibt = "--include-border-title" if self.params["pcb_pdf"]["include_border_title"] else ""
            edge = self.params["pcb_pdf"]["edge_layer"]

            for layer in self.params["pcb_pdf"]["front_layers"]:
                file_path = self.kdocpath(f"{{2D}}/{{KNAME}}_{layer}.pdf", docheck)
                if file_path != None and self.go_ahead (file_path, False, docheck):
                    if doing:
                        print (doing)
                        doing = ""
                    layers = layer if edge == "" else f"{layer},{edge}"
                    subprocess.run(self.kdreplace(
                        f"{self.params['kicad_cli_cmd']} pcb export pdf {bandw} {ev} {er} {ibt} {theme} --layers '{layers}' -o '{file_path}' '{{PCB}}'"), shell=True, check=docheck)
                    self.appfile (file_path)

            for layer in self.params["pcb_pdf"]["back_layers"]:
                file_path = self.kdocpath(f"{{2D}}/{{KNAME}}_{layer}.pdf", docheck)
                if file_path != None and self.go_ahead (file_path, False, docheck):
                    if doing:
                        print (doing)
                    layers = layer if edge == "" else f"{layer},{edge}"
                    subprocess.run(self.kdreplace(
                        f"{self.params['kicad_cli_cmd']} pcb export pdf {bandw} {ev} {er} {ibt} {theme} --mirror --layers '{layers}' -o '{file_path}' '{{PCB}}'"), shell=True, check=docheck)
                    self.appfile (file_path)

        return

    def bom(self, docheck: bool) -> None:
        if not self.enabled ("bom"):
            return
        if not os.path.exists(self.params['bom']['path']):
            print (f"*** {self.params['bom']['path']} not found")
            if docheck:
                sys.exit("*** Aborting")
            return

        file_path = self.kdocpath(f"{{BOM}}/{{KNAME}}_bom.{self.params['bom']['ext']}", docheck)
        if file_path != None and self.go_ahead (file_path, True, docheck):
            print ("*** Doing BOM")
            # first make the netlist
            netlist_file = self.kdocpath(f"{{BOM}}/{{KNAME}}_netlist.xml", docheck)
            if netlist_file != None:
                subprocess.run(self.kdreplace(
                    f"{self.params['kicad_cli_cmd']} sch export netlist --format kicadxml -o '{netlist_file}' '{{SCH}}'"), shell=True, check=docheck)
                subprocess.run(
                    f"/usr/bin/python3 {self.params['bom']['path']} {self.params['bom']['args']} '{netlist_file}' '{file_path}'", shell=True, check=docheck)
                # delete the netlist
                os.remove(netlist_file)
                self.appfile (file_path)
            else:
                print ("*** Netlist file not created, cannot create BOM")
                if docheck:
                    sys.exit("*** Aborting")
                return
        return

    def ibom(self, docheck: bool) -> None:
        if not self.enabled ("ibom"):
            return
        if not os.path.exists(self.params['ibom']['path']):
            print (f"*** {self.params['ibom']['path']} not found")
            if docheck:
                sys.exit("*** Aborting")
            return

        file_path = self.kdocpath (f"{{BOM}}/{{KNAME}}_ibom.html", docheck)
        if file_path != None and self.go_ahead (file_path, False, docheck):
            print ("*** Doing interactive BOM")
            dark = "--dark-mode" if self.params["ibom"]["dark_mode"] else ""
            subprocess.run(self.kdreplace(
                f"/usr/bin/python3 {self.params['ibom']['path']} {dark} --no-browser --dest-dir '{os.path.abspath(self.bom_dir)}' --name-format '{{KNAME}}_ibom' '{{PCB}}'"), shell=True, check=docheck)
            self.filelist.append  (file_path)
        return
                
    def gerbers_kcli(self, do_gerb_gen: bool, archive_dir: str, zip_file: str, docheck: bool) -> None:
        """`gerbers_kcli` generates gerbers using kicad-cli"""

        # gerbers first and zip later        
        if not archive_dir[-1] == "/":
            archive_dir += "/"
        doing = "*** Doing Gerbers"
        if do_gerb_gen:
            print (doing)
            doing = ""
            if not os.path.exists(archive_dir):
                os.makedirs(archive_dir)

            # gerbers
            layers_as_str = ",".join(self.params["gerbers"]["layers"])
            er = "--exclude-refdes" if self.params["gerbers"]["exclude_refdes"] else ""
            ev = "--exclude-value" if self.params["gerbers"]["exclude_value"] else ""
            ibt = "--include-border-title" if self.params["gerbers"]["include_border_title"] else ""
            npe = "--no-protel-ext" if self.params["gerbers"]["no-protel-ext"] else ""
            subprocess.run(self.kdreplace(
                f"{self.params['kicad_cli_cmd']} pcb export gerbers {er} {ev} {ibt} {npe} --layers {layers_as_str} -o '{archive_dir}' '{{PCB}}'"), shell=True, check=docheck)
            # drill files
            subprocess.run(self.kdreplace(
                f"{self.params['kicad_cli_cmd']} pcb export drill --excellon-units mm --generate-map --map-format gerberx2 -o '{archive_dir}' '{{PCB}}'"), shell=True, check=docheck)
            if self.params["gerbers"]["compress"] < 2:
                self.appfile (archive_dir)
                
        # zip em up
        if self.params["gerbers"]["compress"] > 0:
            if do_gerb_gen or self.go_ahead (zip_file, False, docheck):
                if doing:
                    print (doing)
                subprocess.run(f"zip '{zip_file}' '{archive_dir}'/*", shell=True, check=docheck)
                if self.params["gerbers"]["compress"] > 0:
                    self.appfile (zip_file)
            if self.params["gerbers"]["compress"] > 1:
                if doing:
                    print (doing)
                shutil.rmtree(archive_dir)
        
        return
    
    def gerbers_kikit(self, do_gerb_gen: bool, archive_dir: str, zip_file: str, docheck: bool) -> None:
        """`gerbers_kikit` generates gerbers using kikit"""

        if not os.path.exists(self.params['kikit_path']):
            print ("*** Not a valid path for KiKit, not generating Gerbers", self.params['kikit_path'])
            if docheck:
                sys.exit("*** Aborting")
            return
        else:
            # KiKit generates both archive folder and zip file
            if do_gerb_gen:
                print ("*** Doing Gerbers")
                if os.path.exists(archive_dir):
                    shutil.rmtree(archive_dir)
                drc = "--no-drc" if not self.params["gerbers"]["drc"] else ""
                options = self.params["gerbers"]["options"]
                subprocess.run (self.kdreplace(f"{self.params['kikit_path']} fab {self.params['gerbers']['fabhouse']} --nametemplate {{KNAME}}_{{}} {drc} {options} '{{PCB}}' '{{GERBERS}}'"), shell=True, check=docheck)
                if not os.path.isdir(self.kdreplace(f"{{GERBERS}}/gerber")):
                    print ("*** No Gerbers generated")
                    if docheck:
                        sys.exit("*** Aborting")
                else:
                    os.rename(self.kdreplace(f"{{GERBERS}}/gerber"), archive_dir)
                if self.params["gerbers"]["compress"] < 2:
                    self.appfile (archive_dir)
                if self.params["gerbers"]["compress"] > 0:
                    self.appfile (zip_file)
            # Remove unwanted folder or zip file
            if self.params["gerbers"]["compress"] == 0 and os.path.isfile (zip_file):
                os.remove (zip_file)
            elif self.params["gerbers"]["compress"] > 1 and os.path.isdir (archive_dir):
                shutil.rmtree(archive_dir)

            return
        
    def gerbers(self, docheck: bool) -> None:
        if not self.enabled ("gerbers"):
            return
        archive_dir = self.kdocpath(f"{{GERBERS}}/{{KNAME}}_gerbers", docheck)
        zip_file = self.kdocpath(f"{{GERBERS}}/{{KNAME}}_gerbers.zip", docheck)
        
        if self.params["gerbers"]["compress"] < 2:
            do_gerb_gen = archive_dir != None and self.go_ahead (archive_dir, False, docheck)
        else:
            do_gerb_gen = zip_file != None and self.go_ahead (zip_file, False, docheck)

        if self.params['gerbers']['kikit']:
            return self.gerbers_kikit(do_gerb_gen, archive_dir, zip_file, docheck)
        else:
            return self.gerbers_kcli(do_gerb_gen, archive_dir, zip_file, docheck)


    def step(self, docheck: bool) -> None:
        if not self.enabled ("step"):
            return
        file_path = self.kdocpath (f"{{3D}}/{{KNAME}}.step", docheck)
        if file_path != None and self.go_ahead (file_path, False, docheck):
            print ("*** Doing step")
            subprocess.run(self.kdreplace(
                f"{self.params['kicad_cli_cmd']} pcb export step --force --subst-models -o '{file_path}' '{{PCB}}'"), shell=True, check=docheck)
            self.appfile (file_path)
        return

    def readme(self) -> str:
        """`readme` returns this project's info"""
        if not self.enabled ("readme"):
            return ""

        readme_content = self.kdreplace (f"## {{KNAME}}\n\n")
        twod = self.kdreplace(self.twod_dir)
        projd = self.kdreplace(self.proj_dir)
        pre = os.path.relpath (twod, projd)
        readme_content += self.kdreplace (f"![PCB front]({pre}/{{KNAME}}_front.svg)\n")
        readme_content += self.kdreplace (f"![PCB back]({pre}/{{KNAME}}_back.svg)\n")
        readme_content += "\n"
        return readme_content

    def check_sch_file (self) -> bool:
        """`check_sch_file` sees if sch file exists"""
        if self.sch_exists == None:
            if not os.path.isfile(self.sch_file):
                print (self.kdreplace("*** {{SCH}} does not exist"))
                self.sch_exists = False
            else:
                self.sch_exists = True
                self.sch_stamp = os.path.getmtime(self.sch_file)
        return self.sch_exists

    def check_pcb_file (self, base: bool, docheck: bool) -> bool:
        """`check_pcb_file` sees if PCB file exists, or if it can create it by separation"""
        """check primary file if self.pcbi==0 or base==True, else both primary and separated file"""

        # Check primary
        if self.pcb_exists[0] == None:
            if not os.path.isfile(self.pcb_file):
                print (f"*** {self.pcb_file} does not exist")
                self.pcb_exists[0] = False
            else:
                self.pcb_exists[0] = True
                self.pcb_stamp = os.path.getmtime(self.pcb_file)
        
        if self.pcbi == 0 or base:
            return self.pcb_exists[0]

        # Check secondary
        if not self.pcb_exists[0]:
            self.pcb_exists[1] = False
        elif self.pcb_exists[1] == None:
            if not os.path.exists(self.params['kikit_path']):
                print ("*** Not a valid path for KiKit, not separating", self.params['kikit_path'])
                if docheck:
                    sys.exit("*** Aborting")
                self.pcb_exists[1] = False
            else: # separated file
                sk_file = self.sep_k_pcb_file(self.pcbi)
                sk_dir = self.sep_k_dir(self.pcbi)
                sk_filename = os.path.basename(sk_file)
                if not os.path.isdir(sk_dir):
                    os.mkdir(sk_dir)
                print (f"*** Separating to make {sk_filename}")
                sepsel = self.params["separation"][self.pcbi-1][1]
                subprocess.run (f"{self.params['kikit_path']} separate --source '{sepsel}' '{self.pcb_file}' '{sk_file}'", shell=True, check=docheck)
                if not os.path.isfile(sk_file):
                    print (f"*** Separation failed to make {sk_filename}")
                    if docheck:
                        sys.exit("*** Aborting")
                    self.pcb_exists[1] = False
                else:
                    self.pcb_exists[1] = True

        return self.pcb_exists[1]

