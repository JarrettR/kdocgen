# kdocgen changelog

## 9 February 2024
* `kdocgenrc.json` searched for and takes precedence over `.kdocgenrc.json`.
* Abort after any CLI error, unless `--cont` specified.
* `--filesmade` and `--filesmade_ignore` options to create file lists for `git add`
* `kcommit.sh` script added to run `kdocgen` and add its outputs before `git commit`
* Make `kicad-cli` command a parameter (so e.g. can set to `kicad-cli-nightly`).

## 10 October 2023
* Print notification of each action
* Deprecate `enabled` list in favor of e.g. `readme["enabled"]` etc.

## 8 October 2023
* Clean up RC file error handling
* Notify user of non global RC files when found

## 7 July 2023
* Change how PCB layers are handled for PDF exports. Instead of a single list of `layers` in the config files there are two lists, `front-layers` and `back-layers`; there also is an `edge-layer`. The specified edge layer is included in each PDF. The front layers are exported normally and the back layers are exported mirrored. (The `--mirror` option is new with kicad-cli 7.0.6.) Note this means old config files specifying `layers` must be modified to work with this update.
* Add `no-protel-ext` option for gerbers. This is a newly available option in kicad-cli 7.0.6
* Change `--units` to `--excellon-units` in drill file command. This reflects a change in kicad-cli.
* More gracefully handle JSON load errors
* Do not attempt to start generating README if `"readme"` is not in `enabled`
