# Configuration files

The script's behavior is controlled by parameters in configurations files. It looks for these in three places:

* ` ~/kdocgenrc.json` or ` ~/.kdocgenrc.json` (unhidden file overrides any hidden file)
*  `dir/kdocgenrc.json` or `dir/.kdocgenrc.json` where `dir` is the project directory specified on the command line. Overrides parameter values from the above file.
*  `kicad_dir/kdocgenrc.json` or `kicad_dir/.kdocgenrc.json` where `kicad_dir` is the directory containing the `kicad_pro` file which is currently being processed. Overrides parameter values from the above two files.

## Terminology
In this document and in the script, *project* refers to a directory, probably corresponding to a git repo, containing all files relating to a particular thing, e.g. a synthesizer module.

*k-project* refers to a directory containing all files collectively describing a KiCad design: e.g. myproject.kicad_pro, myproject.kicad_sch, myproject.kicad_pcb, and so forth.

*A project will contain at least one, but possibly more than one, k-projects.* For instance, one for a synth module's PCB(s) and one for the front panel.

Within a project there will be several directories we need to refer to:

* **`PROJ`** is the project directory.
* **`DOCS`** is the directory where documentation (for *all* k-projects) is written.
* **`2D`**, **`3D`**, and **`BOM`** are directories for categories of documentation.
* **`KPROJ`** is a directory containing a k-project.
* **`KPROJUP`** is the parent directory of  `KPROJ`.
* **`GERB`** is the directory to which is written a Gerbers file directory and/or compressed Gerbers file.

## Configuration file contents

The configuration files are in JSON format. They need not contain all the following entries. Missing entries take the values given in a configuration file at the previous level, or built in default values.

An example configuration file is in [`kdocgenrc.json`](kdocgenrc.json).

### Top level entries:

|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`\*|list|`schem_pdf`, `ibom`, `bom`, `pcb_svg`, `pcb_pdf`, `gerbers`, `step`, `readme`|Items to be output|all|
|`docs_dir`|single|path relative to `PROJ`\*\* \*\*\*|`DOCS`|`"docs"`|
|`2d_dir`|single|path relative to `DOCS`\*\*|`2D`|`"2D"`|
|`3d_dir`|single|path relative to `DOCS`\*\*|`3D`|`"3D"`|
|`bom_dir`|single|path relative to `DOCS`\*\*|`BOM`|`"BOM"`|
|`gerb_dir`|single|path relative to `PROJ`\*\* \*\*\*|`GERB`|`"Gerbers"`|
|`kicad_cli_cmd`|single|CLI command|command for kicad-cli|`"kicad-cli"`|
|`kikit_path`|single|absolute path|path to KiKit executable| `"/usr/local/bin/kikit"` |
|`separation`|list|`[[name, spec]...]`|Names and specifiers for multi board separation\*\*\*\*|`[ ]`|
|`schem_pdf`|structure||parameters for `schem_pdf`| - |
|`ibom`|structure||parameters for `ibom`| - |
|`bom`|structure||parameters for `bom`| - |
|`pcb_svg`|structure||parameters for `pcb_svg`| - |
|`pcb_pdf`|structure||parameters for `pcb_pdf`| - |
|`gerbers`|structure||parameters for `gerbers`| - |
|`step`|structure||parameters for `step`| - |
|`readme`|structure||parameters for `readme`| - |

\* This usage is deprecated; instead use `enabled` parameter for each item (see below).

\*\* If a directory path contains `"{NAME}"` or `"{KNAME}"` then this is replaced with the name of the main project or the k-project respectively. This allows for instance separate `2D` directories for different k-projects.

\*\*\* If `docs_dir` or `gerb_dir` is of the form `"some/path/here"` then it refers to a path relative to `PROJ`. If it begins with `"{PROJ}"` or `"{PROJUP}"` or `"{KPROJUP}"` it refers to a path relative to `PROJ` or `KPROJ`  or `KPROJUP`.

For example, if  `PROJ` is `./example` and `KPROJ` is `./example/folder/kicadproject`:

* `"docs_dir": "docs"` refers to `./example/docs`
* `"docs_dir": "{PROJ}/docs"` also refers to `./example/docs`
* `"docs_dir": "{KPROJ}/docs"` refers to `./example/folder/kicadproject/docs`
* `"docs_dir": "{KPROJUP}/docs"` refers to `./example/folder/docs`

\*\*\*\* Multi board PCBs can be separated and processed separately if you have [KiKit](https://yaqwsx.github.io/KiKit/v1.3/) installed. Each entry in the `separation` list is a 2-element list consisting of a unique name for that PCB and the parameter to pass to KiKit to specify the bounds of that board. This is most likely useful only in the configuration file in the `KPROJ` directory. Example:

```
{
    "separation": [
	["B1", "annotation; ref: B1"],
	["B2", "annotation; ref: B2"]
	]
}
```


### Entries for `schem_pdf`: 
|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`|single|bool|this action is enabled|`true`|

### Entries for `ibom`: 

|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`|single|bool|this action is enabled|`true`|
|`path`|single|absolute path|path to executable|`"/usr/local/bin/generate_interactive_bom.py"`|
|`dark_mode`|single|bool|dark mode on or off|`true`|

(The `dark_mode` parameter seems not to work correctly in Google Chrome.)

### Entries for `bom`:

|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`|single|bool|this action is enabled|`true`|
|`path`|single|absolute path|path to BOM executable|`"/usr/share/kicad/plugins/bom_csv_grouped_extra.py"`|
|`args`|single|string|arguments|`""`|
|`ext`|single|string|extension for output file|`"csv"`|

### Entries for `pcb_svg`:

|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`|single|bool|this action is enabled|`true`|
|`front_layers`|list|layer names|layers for front SVG|`["Edge.Cuts","F.SilkS","F.Cu"]`|
|`back_layers`|list|layer names|layers for back SVG|`["Edge.Cuts","B.SilkS","B.Cu"]`|
|`theme`|single|theme name|color theme to use|`""`|
|`black_and_white`|single|bool|black and white only or not|`false`|
|`page_size_mode`|single|number|`0` = page with frame and title block, `1` = current page size, `2` = board area only|`2`|
|`exclude_drawing_sheet`|single|bool|exclude drawing sheet or no|`true`|

### Entries for `pcb_pdf`:

|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`|single|bool|this action is enabled|`true`|
|`front_layers`|list|layer names|front layers to plot|`["F.Cu", "In1.Cu", "In2.Cu", "F.SilkS", "F.Paste", "F.Mask"]`|
|`back_layers`|list|layer names|back layers to plot mirrored|`["B.Cu", "B.SilkS", "B.Paste", "B.Mask"]`|
|`edge_layer`|single|layer name|edge layer to include with each|`"Edge.Cuts"`|
|`exclude_refdes`|single|bool|exclude the reference designator text or no|`true`|
|`exclude_value`|single|bool|exclude the value text or no|`true`|
|`include_border_title`|single|bool|Include the border and title block or no|`true`|
|`theme`|single|theme name|color theme to use|`""`|
|`black_and_white`|single|bool|black and white only or not|`true`|

### Entries for `gerbers`:

|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`|single|bool|this action is enabled|`true`|
|`kikit`|single|bool|use kikit or no|`false`|
|`compress`|single|number|`2`=compress and delete gerber directory; `1`=compress only; `0`=no compress|`2`|

#### Following are used only if not using `kikit`:

|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`layers`|list|layer names|layers to include|[`"F.Cu", "In1.Cu", "In2.Cu", "B.Cu", "F.SilkS", "B.SilkS", "F.Paste", "B.Paste", "F.Mask", "B.Mask", "Edge.Cuts"]`|
|`exclude_refdes`|single|bool|exclude the reference designator text or no|`false`|
|`exclude_value`|single|bpp;|exclude the value text or no|`false`|
|`include_border_title`|single|bool|Include the border and title block or no|`false`|
|`no-protel-ext`|single|bool|Use KiCad gerber file extension|`false`|

#### Following are used only if using `kikit`:

|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`fabhouse`|single|string|fabrication house|`"jlcpcb"`|
|`drc`|single|bool|do DRC or no|`true`|
|`options`|single|string|fab specific options|`""`|

### Entries for `step`: 
|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`|single|bool|this action is enabled|`true`|

### Entries for `readme`: 
|Parameter|Type|Values|Meaning|Default|
|----|----|----|----|----|
|`enabled`|single|bool|this action is enabled|`true`|

